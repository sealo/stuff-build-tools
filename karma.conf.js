const path = require('path');

module.exports = function (options) {
    const basePath = options.basePath;
    const webpackConfig = require(path.resolve(__dirname, 'webpack.config.js'))(options);
    const testPath = path.resolve(basePath, 'src/**/*-test.js');
    const watch = process.argv.indexOf('--watch') > -1;

    return function(config) {
        config.set({
            browsers: ['PhantomJS'],
            frameworks: [ 'mocha', 'chai' ],
            singleRun: !watch,

            files: [testPath],

            preprocessors: {
                [testPath]: ['webpack']
            },

            webpack: webpackConfig,

            webpackMiddleware: {
                noInfo: true
            }
        });
    }
};
