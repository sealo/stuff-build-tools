const path = require('path');

module.exports = {
    makeWebpack: require(path.resolve(__dirname, 'webpack.config.js')),
    makeKarma: require(path.resolve(__dirname, 'karma.conf.js'))
};
