'use strict';

const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

/**
 * Create the webpack.config.js Object
 *
 * @public
 *
 * @param  {Object}  options
 * @param  {String}  options.basePath - root directory
 * @param  {String}  [options.outputFileName] - name of output file in dist
 * @param  {String}  [options.entry] - entry point, uses package.json.entry as default
 * @param  {Object|Array}  [options.plugins] - any additional plugins
 */
module.exports = function makeWebpack (options) {
    const basePath = options.basePath;
    const pkg = require(path.join(basePath, '/package.json'));
    const version = pkg.version;
    const entry = options.entry || path.join(basePath, pkg.main);
    const outputFileName = options.outputFileName || pkg.name.replace(/\s/g, '-');
    const ExtractTextPlugin = require(path.join(basePath, 'node_modules', 'extract-text-webpack-plugin'));
    const autoprefixer = require(path.join(basePath, 'node_modules', 'autoprefixer'));

    const webpackConfig = {
        entry: entry,
        devtool: options.production ? undefined : 'inline-source-map',
        output: {
            path: path.join(basePath, 'dist', version),
            filename: outputFileName + '.js'
        },
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components|config)/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['es2015']
                    }
                },
                {
                    test: /\.scss$/,
                    loaders: [
                        ExtractTextPlugin.extract(path.join(basePath, 'node_modules', 'style-loader')),
                        'css-loader',
                        'postcss-loader'
                    ]
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin(outputFileName + '.css')
        ],
        postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ]
    };

    if (process.env.NODE_ENV === 'production') {
        webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
            mangle: {},
            sourceMap: false
        }))
    }

    if (options.plugins) {
        const plugins = options.plugins;
        if (Array.isArray(plugins)) {
            webpackConfig.plugins = webpackConfig.plugins.concat(plugins);
        } else if('object' === typeof plugins) {
            webpackConfig.plugins.push(plugins);
        }
    }

    // replace correct versions in index.html
    fs.readFile(path.join(basePath, 'test', 'index.html'), 'utf8', function (err, data) {
      if (err) {
        return console.log(err);
      }
      var result = data.replace(/dist\/\d+.\d+.\d+/g, `dist/${pkg.version}`);

      fs.writeFile(path.join(basePath, 'test', 'index.html'), result, 'utf8', function (err) {
         if (err) return console.log(err);
      });
    });

    return webpackConfig;
}
